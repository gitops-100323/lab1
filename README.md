# lab1
## Require
```
brew install terraform
brew install awscli
```
## AWS Auth
```
aws configure
```
## Create EKS Cluster
```
terraform init
terraform plan
terraform apply
```
## Create kubeconfig
```
aws eks --region ap-southeast-1 update-kubeconfig --name gitops80-demo
```
## Connect Git
```
export GITLAB_TOKEN=xxxxxxx
```
## Flux bootstrap
```
flux bootstrap gitlab \
  --owner=gitops-100323 \
  --repository=lab2 \
  --path=./clusters/default \
  --branch=main
```
## Flux reconcile
```
flux reconcile ks flux-system --with-source
```