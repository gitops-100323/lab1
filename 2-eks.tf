resource "aws_iam_role" "eks-iam-role-devops" {
  name = "eks-cluster-${var.environment}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "devops-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks-iam-role-devops.name
}

resource "aws_kms_key" "kms-key1" {
  description             = "GitOps EKS Secret Encryption Key"
  deletion_window_in_days = 7
  enable_key_rotation     = true

  tags = {
    Name = "${var.environment}-kms-1"
  }
}

resource "aws_kms_alias" "kms-name" {
  name          = "alias/${var.environment}-key-1"
  target_key_id = aws_kms_key.kms-key1.key_id
}

resource "aws_eks_cluster" "devops" {
  name     = var.environment
  role_arn = aws_iam_role.eks-iam-role-devops.arn
  version = "1.24"

  vpc_config {
    subnet_ids = [
      aws_subnet.public_subnet.0.id,
      aws_subnet.public_subnet.1.id,
      aws_subnet.private_subnet.0.id,
      aws_subnet.private_subnet.1.id
    ]
  }

  encryption_config {
    resources = ["secrets"]
    provider {
      key_arn = aws_kms_key.kms-key1.arn
    }
  }

  depends_on = [
    aws_iam_role_policy_attachment.devops-AmazonEKSClusterPolicy,
    aws_kms_key.kms-key1
  ]
}

resource "aws_kms_key" "kms-key2" {
  description             = "GitOps EKS Secret Encryption Key"
  deletion_window_in_days = 7
  enable_key_rotation     = true

  tags = {
    Name = "${var.environment}-kms-2"
  }
}

resource "aws_kms_alias" "kms-name-key2" {
  name          = "alias/${var.environment}-key-2"
  target_key_id = aws_kms_key.kms-key2.key_id
}

locals {
  arn-key-2 = aws_kms_key.kms-key2.arn
  arn-oidc = aws_iam_openid_connect_provider.cluster.arn
  id-oidc = aws_iam_openid_connect_provider.cluster.url
}

resource "aws_iam_policy" "policy" {
  name        = "kustomization-controller-${var.environment}"
  path        = "/"
  description = "${var.environment}-policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "kms:Decrypt",
        "kms:DescribeKey"
      ],
      "Effect": "Allow",
      "Resource": "${local.arn-key-2}"
    }
  ]
}
EOF
  depends_on = [aws_kms_key.kms-key2]
}

data "aws_region" "current" {}

data "tls_certificate" "devops" {
  url = aws_eks_cluster.devops.identity.0.oidc.0.issuer
}
### OIDC config
resource "aws_iam_openid_connect_provider" "cluster" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.tls_certificate.devops.certificates[0].sha1_fingerprint]
  url             = aws_eks_cluster.devops.identity[0].oidc[0].issuer 
  tags = {
    "alpha.eksctl.io/cluster-name" = "${var.environment}",
  }
}

resource "aws_iam_role" "eks-iam-serviceaccount" {
  name = "eks-iam-serviceaccount-flux-${var.environment}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "${local.arn-oidc}"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "${local.id-oidc}:sub": "system:serviceaccount:flux-system:kustomize-controller",
          "${local.id-oidc}:aud": "sts.amazonaws.com"
          }
        }
    }
  ]
}
POLICY
  tags = {
    "alpha.eksctl.io/cluster-name" = "${var.environment}"
    "alpha.eksctl.io/iamserviceaccount-name" = "flux-system/kustomize-controller"
    "eksctl.cluster.k8s.io/v1alpha1/cluster-name" = "${var.environment}"
  }
}

resource "aws_iam_role_policy_attachment" "serviceaccount-AmazonEKSClusterPolicy" {
  policy_arn = aws_iam_policy.policy.arn
  role       = aws_iam_role.eks-iam-serviceaccount.name
}
