resource "aws_iam_role" "devops-nodes" {
  name = "eks-node-group-${var.environment}-nodes"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "devops-nodes-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.devops-nodes.name
}

resource "aws_iam_role_policy_attachment" "devops-nodes-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.devops-nodes.name
}

resource "aws_iam_role_policy_attachment" "devops-nodes-EC2InstanceProfileForImageBuilderECRContainerBuilds" {
  policy_arn = "arn:aws:iam::aws:policy/EC2InstanceProfileForImageBuilderECRContainerBuilds"
  role       = aws_iam_role.devops-nodes.name
}

resource "aws_iam_role_policy_attachment" "devops-nodes-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.devops-nodes.name
}

resource "aws_eks_node_group" "private-devops-nodes" {
  cluster_name    = aws_eks_cluster.devops.name
  node_group_name = "private-${var.environment}-nodes"
  node_role_arn   = aws_iam_role.devops-nodes.arn

  subnet_ids = [
    aws_subnet.private_subnet.0.id,
    aws_subnet.private_subnet.1.id
  ]

  capacity_type  = "ON_DEMAND"
  instance_types = ["t3.medium"]

  scaling_config {
    desired_size = 1
    max_size     = 3
    min_size     = 0
  }

  update_config {
    max_unavailable = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.devops-nodes-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.devops-nodes-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.devops-nodes-AmazonEC2ContainerRegistryReadOnly,
  ]
}

